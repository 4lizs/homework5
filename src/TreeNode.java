package src;

import java.util.*;

public class TreeNode {

	private String name;
	private TreeNode firstChild;
	private TreeNode nextSibling;

	TreeNode() {

	}

	TreeNode(String s) {
		this.setName(s);
	}

	public TreeNode(String n, TreeNode d, TreeNode r) {
		this.setName(n);
		this.setFirstChild(d);
		this.setNextSibling(r);

	}

	public static TreeNode parsePrefix(String s) {
		if (s.trim().contains("()")
				|| !s.trim().contains("(")
				|| s.contains(" ")
				|| s.contains(",C(")
				|| s.contains("((")
				|| s.contains("\t")
				|| s.trim().contains(",,")
				|| (!s.contains("(") && !s.contains(")") && s.contains(","))

				){
			throw new RuntimeException("vale sisend " + s);
		}
		if (s != null && !s.isEmpty()) {
			TreeNode root = new TreeNode();
			StringBuffer b = new StringBuffer();
			StringTokenizer st = new StringTokenizer(s.trim(), "()", true);
			while(st.hasMoreTokens()){
				String current = st.nextToken();
				if(current.equals("(")){
					root.name = b.toString();
					buildTree(s.substring(current.length()), root, true);
					b.setLength(0);
					break;
			} else{
				b.append(current);
			}
				
				}
			if (b.length() > 0 && root.firstChild == null)
				root.name = b.toString();
			return root;
	}
		else
			throw new RuntimeException("Can't parse an empty string.");
	}

	public static void buildTree(String s, TreeNode node, boolean root) {
		StringBuffer b = new StringBuffer();
		StringTokenizer st = new StringTokenizer(s.trim(), "()", true);
		while (st.hasMoreTokens()) {
			String curr = st.nextToken();
			if(curr.equals("(")){
					if (b.length() > 0) {
						node = create(node, b.toString(), root);
						if (root)
							node = node.firstChild;
						else
							node = node.nextSibling;
						b.setLength(0);
					}
					break;
			}
			if (curr.equals(",")){
				
				if (b.length() > 0) 
					node = create(node, b.toString(), root);
			}
			if(curr.equals(")")){
				
				if (b.length() > 0)
					node = create(node, b.toString(), root);
			} else{
				b.append(curr);
				break;
				
			}
			
		}
		if (b.length() > 0)
			node = create(node, b.toString(), root);
		
	}

	private static TreeNode create(TreeNode node, String name, boolean root) {
		if (root)
			node.firstChild = new TreeNode(name);
		else
			node.nextSibling = new TreeNode(name);
		return node;
	}
	
	public String rightParentheticRepresentation() {
		StringBuffer b = new StringBuffer();
		TreeNode node = firstChild;
		if (firstChild != null)
			b.append('(');
		while (node != null) {
			b.append(node.rightParentheticRepresentation());
			node = node.nextSibling;
			if (node != null)
				b.append(',');
		}
		if (getNextSibling() != null)
			b.append(')');
		return b.append(name).toString();
	
	}


	public static void main(String[] param) {
		String s = "A(B1,C,D)";
		TreeNode t = TreeNode.parsePrefix(s);
		String v = t.rightParentheticRepresentation();
		System.out.println(s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
	}

	public TreeNode getFirstChild() {
		return firstChild;
	}

	public void setFirstChild(TreeNode firstChild) {
		this.firstChild = firstChild;
	}

	public TreeNode getNextSibling() {
		return nextSibling;
	}

	public void setNextSibling(TreeNode nextSibling) {
		this.nextSibling = nextSibling;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
